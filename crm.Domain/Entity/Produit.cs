﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crm.Domain.Entity
{


    [Table("Produit")]
    public class Produit
    {

        [Key]
        public int ProduitId { get; set; }
        [Required, MaxLength(15)]
        [Display(Name = "Produit")]
        public string nomProd { get; set; }
        [Required]
        public string unite { get; set; }
        [Required]
        [Range(0, double.MaxValue)]
        [Display(Name = "Quantité")]
        public int qte { get; set; }
        [Required]
        [Range(0, double.MaxValue)]

        [Display(Name = "Quantité Min")]
        public int qteMin { get; set; }
        [Required]
        [Range(0, double.MaxValue)]
        public int prix { get; set; }


        [DataType(DataType.MultilineText)]
        [Display(Name = "Description")]

        public string description { get; set; }



        public string image { get; set; }

        [DataType(DataType.DateTime), DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime DateAjout { get; set; }
        public ICollection<Promotion> Promotions { get; set; }
        public ICollection<PackProduct> PackProduct { get; set; }






        public int CategProdFK { get; set; }
        [ForeignKey("CategProdFK")]
        public CategorieProd categorie { get; set; }



        public Magasin magasin { get; set; }
        public int MagasinId { get; set; }



        public Produit() { }






    }
}
