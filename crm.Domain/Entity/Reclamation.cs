﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crm.Domain.Entity
{
    public enum TypeRec
    {
        Technique = 1, Financiere = 2, Relationnelle = 3
    }
    

    [Table("Reclamation")]
    public class Reclamation
    {

        [Key]
        public int Reclamationid { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "Choisir un type")]
        public TypeRec type { get; set; }
        public string description { get; set; }
        public string etat { get; set; }

        public int ClientFK { get; set; }
        [ForeignKey("ClientFK")]
        public User client { get; set; }

        public int AdminFK { get; set; }
        [ForeignKey("AdminFK")]
        public User admin { get; set; }


        public int? ProduitFK { get; set; }
        [ForeignKey("ProduitFK")]
        public Produit produit { get; set; }



        public string reponse { get; set; }
        [DataType(DataType.Date)]
        public DateTime DateReclamation { get; set; }



        public Reclamation() { }






    }
}
