﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crm.Domain.Entity
{
    [Table("Posts")]
    public class Post
    {
        [Key]
        public int id { get; set; }
        [Required(ErrorMessage = "Please enter Post Title.")]
        [ConcurrencyCheck]
        [MaxLength(24), MinLength(5)]
        public string Title { get; set; }
        [Required(ErrorMessage = "Please enter Post Body.")]
        public string body { get; set; }
        public DateTime date { get; set; }
        public String Image { get; set; }
        public String path { get; set; }
        public int likes { get; set; }
        public ICollection<Comment> comments { get; set; }
        
    }
}
