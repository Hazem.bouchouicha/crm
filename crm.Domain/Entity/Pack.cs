﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crm.Domain.Entity
{


    [Table("Pack")]
    public class Pack
    {

        [Key]
        public int PackId { get; set; }
        [Required, MaxLength(15)]
        public string NomPack { get; set; }



        [Required(ErrorMessage = " champs obligatoire"), MaxLength(50)]
        [Display(Name = "Description du Pack")]
        public string Description { get; set; }



        public string PromoImage { get; set; }
        [DataType("datetime2")]
        public DateTime? PackDateDebut { get; set; }
        [DataType("datetime2")]
        public DateTime? PackDateFin { get; set; }

        [Required]
        public int PackPrix { get; set; }









        public ICollection<PackProduct> PackProduct { get; set; }





        public Pack() { }






    }
}
