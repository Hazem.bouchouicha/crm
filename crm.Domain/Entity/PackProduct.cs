﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crm.Domain.Entity
{


    [Table("PackProduct")]
    public class PackProduct
    {








        public Pack pack { get; set; }
        public Produit produit { get; set; }
        [Key]
        [Column(Order = 1)]
        public int ProduitId { get; set; }
        [Key]
        [Column(Order = 2)]
        public int PackId { get; set; }




        public PackProduct() { }

        public PackProduct(Pack pack, Produit produit, int produitId, int packId)
        {
            this.pack = pack;
            this.produit = produit;
            ProduitId = produitId;
            PackId = packId;
        }
    }
}
