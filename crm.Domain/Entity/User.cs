﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crm.Domain.Entity
{


    [Table("User")]
    public class User
    {

        [Key]
        public int Userid { get; set; }


        public String Nom { get; set; }

        public String Prenom { get; set; }

        public String Tel { get; set; }
        public String Adresse { get; set; }
        public String MDP { get; set; }
        public ICollection<Reservation> Reservations { get; set; }



        public User()
        {

        }


    }
}
