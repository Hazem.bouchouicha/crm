﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crm.Domain.Entity
{
    [Table("Event")]
    public class Event
    {


        [Key]
        public int EventId { get; set; }

        [Display(Name = "Name Event")]
        [Required(ErrorMessage = "this field is required")]

        [DataType(DataType.Text)]
        public String Nom_Event { get; set; }

        public DateTime dateDebut { get; set; }
        [Display(Name = "Place")]
        [DataType(DataType.Text)]

        [Required(ErrorMessage = "this field is required")]
        public String Place { get; set; }
        [Required(ErrorMessage = "this field is required")]

        public int Nb_Place { get; set; }
        public String type { get; set; }
        // public List<User> listUser { get; set; }

        public ICollection<Reservation> Reservations { get; set; }

        public ICollection<Rating> Rating { get; set; }

        public Event()
        {

        }


    }
}
