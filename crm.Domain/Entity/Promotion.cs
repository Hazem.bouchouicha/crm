﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crm.Domain.Entity
{


    [Table("Promotion")]
    public class Promotion
    {

        [Key]
        public int PromoId { get; set; }


        [Required, MaxLength(15)]
        public string NomPromo { get; set; }



        [Required(ErrorMessage = " champs obligatoire"), MaxLength(50)]
        [Display(Name = "Description du Promotion")]
        public string Description { get; set; }



        public string PromoImage { get; set; }
        
        public DateTime PrDateDebut { get; set; }
      
        public DateTime PrDateFin { get; set; }

        [Required]
        public int PromoPrix { get; set; }









        public Produit produit { get; set; }
        public int ProduitId { get; set; }





        public Promotion() { }

        public Promotion(int promoId, string nomPromo, string description, string promoImage, DateTime prDateDebut, DateTime prDateFin, int promoPrix, Produit produit, int produitId)
        {
            PromoId = promoId;
            NomPromo = nomPromo;
            Description = description;
            PromoImage = promoImage;
            PrDateDebut = prDateDebut;
            PrDateFin = prDateFin;
            PromoPrix = promoPrix;
            this.produit = produit;
            ProduitId = produitId;
        }
    }
}
