﻿using crm.Data.infrastructure;
using crm.Domain.Entity;
using crm.ServicesPattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crm.Services
{
    public class PromotionService : Service<Promotion>, IPromotionService
    {
        static IDataBaseFactory Factory = new DataBaseFactory();
        static IUnitOfWork Uok = new UnitOfWork(Factory);
        public PromotionService() : base(Uok)
        {
        }
        public IEnumerable<Promotion> PromotionsValables()
        {
            return GetMany(p => (DateTime.Now - p.PrDateFin).Days > 0);

        }
    }
}
