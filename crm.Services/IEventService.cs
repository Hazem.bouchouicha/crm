﻿using System;
using crm.Domain.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using crm.ServicesPattern;

namespace crm.Services
{
    public interface IEventService : IService<Event>
    {
        int ReservationByEvent(int id);

    }
}
