﻿using crm.Data.infrastructure;
using crm.Domain.Entity;
using crm.ServicesPattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crm.Services
{
public  class CommentService : Service<Comment>,ICommentService
    {
        static IDataBaseFactory Factory = new DataBaseFactory();

        static IUnitOfWork utk = new UnitOfWork(Factory);

        public CommentService() : base(utk)
        {
        }
    }
}
