﻿using crm.Domain.Entity;
using crm.ServicesPattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crm.Services
{
   public  interface ICommentService : IService<Comment>
    {
    }
}
