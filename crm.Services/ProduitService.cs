﻿using crm.Data.infrastructure;
using crm.Domain.Entity;
using crm.ServicesPattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crm.Services
{
    public class ProduitService : Service<Produit>, IProduitService
    {


        static IDataBaseFactory Factory = new DataBaseFactory();
        static IUnitOfWork Uok = new UnitOfWork(Factory);
        public ProduitService() : base(Uok)
        {
        }

       
    }
}
