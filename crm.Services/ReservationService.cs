﻿using crm.ServicesPattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using crm.Data.infrastructure;
using crm.Domain.Entity;

namespace crm.Services
{
    public class ReservationService : Service<Reservation>, IReservationService
    {
        static IDataBaseFactory Factory = new DataBaseFactory();
        static IUnitOfWork Uok = new UnitOfWork(Factory);

        public ReservationService() : base(Uok)
        {
        }

        public ReservationService(IUnitOfWork utk) : base(utk)
        {
        }

        public int UserId
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }
    }
}
