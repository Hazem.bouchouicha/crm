﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using crm.Data.infrastructure;
using crm.Domain.Entity;
using crm.ServicesPattern;
using System.Data.SqlClient;
using System.Data;

namespace crm.Services
{
    public class EventService : Service<Event>, IEventService
    {
        static IDataBaseFactory Factory = new DataBaseFactory();
        static IUnitOfWork Uok = new UnitOfWork(Factory);

        public EventService(IUnitOfWork utk) : base(utk)
        {
        }

        public EventService() : base(Uok)
        {
        }

        public virtual int ReservationByEvent(int id)
        {


            int count = 0;
            List<int> counts = new List<int>();
            string connectionString = @"Data Source=.;Initial Catalog=master;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
            using (SqlConnection s = new SqlConnection(connectionString))
            {
                s.Open();
                using (
                    SqlCommand cmdSelect = new SqlCommand("SELECT s.EventId , COUNT(s.ResId) as count FROM  [crmBase3].[dbo].[Event] s  where s.EventId =" + @id + "group by s.EventId;", s))
                {
                    cmdSelect.CommandType = CommandType.Text;
                    SqlParameter param = new SqlParameter();
                    param.ParameterName = "@id";
                    param.Value = id;
                    using (SqlDataReader sqlDataReader = cmdSelect.ExecuteReader())
                    {
                        while (sqlDataReader.Read())
                        {
                            //counts.Add(
                            count = Int32.Parse(sqlDataReader["count"].ToString());
                            // count = sqlDataReader.GetOrdinal("count");
                            //);
                        }
                    }
                }
            }

            return count;
        }

    }
}
