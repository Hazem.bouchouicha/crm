﻿using crm.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crm.Data.configurations
{
    public class ProduitConfig : EntityTypeConfiguration<Produit>
    {
        public ProduitConfig()
        {
            HasMany(e => e.Promotions).WithRequired(t => t.produit).HasForeignKey(e => e.ProduitId);
            HasMany(e => e.PackProduct).WithRequired(t => t.produit).HasForeignKey(e => e.ProduitId);

        }

    }
}
