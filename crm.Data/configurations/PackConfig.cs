﻿using crm.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crm.Data.configurations
{
    class PackConfig : EntityTypeConfiguration<Pack>
    {

        public PackConfig()
        {
            
            HasMany(e => e.PackProduct).WithRequired(t => t.pack).HasForeignKey(e => e.PackId);

        }
    }
}
