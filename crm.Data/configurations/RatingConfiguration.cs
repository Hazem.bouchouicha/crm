﻿using crm.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;

using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crm.Data.configurations
{
   public class RatingConfiguration : EntityTypeConfiguration<Rating>
    {
        public RatingConfiguration()
        {
            HasOptional(rt => rt.Event)
              .WithMany(res => res.Rating)
              .HasForeignKey(prod => prod.EventId)
              .WillCascadeOnDelete(true);
        }
    }
}
