namespace crm.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Najla : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CategorieProd",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nomCategorie = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Produit",
                c => new
                    {
                        ProduitId = c.Int(nullable: false, identity: true),
                        nomProd = c.String(nullable: false, maxLength: 15),
                        unite = c.String(nullable: false),
                        qte = c.Int(nullable: false),
                        qteMin = c.Int(nullable: false),
                        prix = c.Int(nullable: false),
                        description = c.String(),
                        image = c.String(),
                        CategProdFK = c.Int(nullable: false),
                        MagasinId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ProduitId)
                .ForeignKey("dbo.Magasin", t => t.MagasinId, cascadeDelete: true)
                .ForeignKey("dbo.CategorieProd", t => t.CategProdFK, cascadeDelete: true)
                .Index(t => t.CategProdFK)
                .Index(t => t.MagasinId);
            
            CreateTable(
                "dbo.Magasin",
                c => new
                    {
                        MagasinId = c.Int(nullable: false, identity: true),
                        nomMagasin = c.String(nullable: false),
                        adresse = c.String(),
                        ville = c.String(),
                    })
                .PrimaryKey(t => t.MagasinId);
            
            CreateTable(
                "dbo.PackProduct",
                c => new
                    {
                        ProduitId = c.Int(nullable: false),
                        PackId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ProduitId, t.PackId })
                .ForeignKey("dbo.Pack", t => t.PackId, cascadeDelete: true)
                .ForeignKey("dbo.Produit", t => t.ProduitId, cascadeDelete: true)
                .Index(t => t.ProduitId)
                .Index(t => t.PackId);
            
            CreateTable(
                "dbo.Pack",
                c => new
                    {
                        PackId = c.Int(nullable: false, identity: true),
                        NomPack = c.String(nullable: false, maxLength: 15),
                        Description = c.String(nullable: false, maxLength: 50),
                        PromoImage = c.String(),
                        PackDateDebut = c.DateTime(precision: 7, storeType: "datetime2"),
                        PackDateFin = c.DateTime(precision: 7, storeType: "datetime2"),
                        PackPrix = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.PackId);
            
            CreateTable(
                "dbo.Promotion",
                c => new
                    {
                        PromoId = c.Int(nullable: false, identity: true),
                        NomPromo = c.String(nullable: false, maxLength: 15),
                        Description = c.String(nullable: false, maxLength: 50),
                        PromoImage = c.String(),
                        PrDateDebut = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        PrDateFin = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        PromoPrix = c.Int(nullable: false),
                        ProduitId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.PromoId)
                .ForeignKey("dbo.Produit", t => t.ProduitId, cascadeDelete: true)
                .Index(t => t.ProduitId);
            
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        ID_Comment = c.Int(nullable: false, identity: true),
                        body = c.String(nullable: false),
                        date = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        ID_Post = c.Int(),
                    })
                .PrimaryKey(t => t.ID_Comment)
                .ForeignKey("dbo.Posts", t => t.ID_Post)
                .Index(t => t.ID_Post);
            
            CreateTable(
                "dbo.Posts",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 24),
                        body = c.String(nullable: false),
                        date = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Image = c.String(),
                        path = c.String(),
                        likes = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Entrée",
                c => new
                    {
                        EntréeId = c.Int(nullable: false, identity: true),
                        Qte = c.Int(nullable: false),
                        prix = c.Int(nullable: false),
                        DateEntrée = c.DateTime(precision: 7, storeType: "datetime2"),
                        produit_ProduitId = c.Int(),
                    })
                .PrimaryKey(t => t.EntréeId)
                .ForeignKey("dbo.Produit", t => t.produit_ProduitId)
                .Index(t => t.produit_ProduitId);
            
            CreateTable(
                "dbo.Event",
                c => new
                    {
                        EventId = c.Int(nullable: false, identity: true),
                        Nom_Event = c.String(nullable: false),
                        dateDebut = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Place = c.String(nullable: false),
                        Nb_Place = c.Int(nullable: false),
                        type = c.String(),
                    })
                .PrimaryKey(t => t.EventId);
            
            CreateTable(
                "dbo.Rating",
                c => new
                    {
                        RatingId = c.Int(nullable: false, identity: true),
                        Note = c.Int(nullable: false),
                        EventId = c.Int(),
                    })
                .PrimaryKey(t => t.RatingId)
                .ForeignKey("dbo.Event", t => t.EventId)
                .Index(t => t.EventId);
            
            CreateTable(
                "dbo.Reservation",
                c => new
                    {
                        ResId = c.Int(nullable: false, identity: true),
                        EventId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ResId)
                .ForeignKey("dbo.Event", t => t.EventId, cascadeDelete: true)
                .ForeignKey("dbo.User", t => t.UserId, cascadeDelete: true)
                .Index(t => t.EventId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.User",
                c => new
                    {
                        Userid = c.Int(nullable: false, identity: true),
                        Nom = c.String(),
                        Prenom = c.String(),
                        Tel = c.String(),
                        Adresse = c.String(),
                        MDP = c.String(),
                    })
                .PrimaryKey(t => t.Userid);
            
            CreateTable(
                "dbo.Reclamation",
                c => new
                    {
                        Reclamationid = c.Int(nullable: false, identity: true),
                        type = c.Int(nullable: false),
                        description = c.String(),
                        etat = c.String(),
                        ClientFK = c.Int(nullable: false),
                        AdminFK = c.Int(nullable: false),
                        ProduitFK = c.Int(),
                        reponse = c.String(),
                        DateReclamation = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Reclamationid)
                .ForeignKey("dbo.User", t => t.AdminFK, cascadeDelete: true)
                .ForeignKey("dbo.User", t => t.ClientFK, cascadeDelete: false)
                .ForeignKey("dbo.Produit", t => t.ProduitFK)
                .Index(t => t.ClientFK)
                .Index(t => t.AdminFK)
                .Index(t => t.ProduitFK);
            
            CreateTable(
                "dbo.Sortie",
                c => new
                    {
                        SortieId = c.Int(nullable: false, identity: true),
                        Qte = c.Int(nullable: false),
                        prix = c.Int(nullable: false),
                        DateSortie = c.DateTime(precision: 7, storeType: "datetime2"),
                        produit_ProduitId = c.Int(),
                    })
                .PrimaryKey(t => t.SortieId)
                .ForeignKey("dbo.Produit", t => t.produit_ProduitId)
                .Index(t => t.produit_ProduitId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Sortie", "produit_ProduitId", "dbo.Produit");
            DropForeignKey("dbo.Reclamation", "ProduitFK", "dbo.Produit");
            DropForeignKey("dbo.Reclamation", "ClientFK", "dbo.User");
            DropForeignKey("dbo.Reclamation", "AdminFK", "dbo.User");
            DropForeignKey("dbo.Reservation", "UserId", "dbo.User");
            DropForeignKey("dbo.Reservation", "EventId", "dbo.Event");
            DropForeignKey("dbo.Rating", "EventId", "dbo.Event");
            DropForeignKey("dbo.Entrée", "produit_ProduitId", "dbo.Produit");
            DropForeignKey("dbo.Comments", "ID_Post", "dbo.Posts");
            DropForeignKey("dbo.Produit", "CategProdFK", "dbo.CategorieProd");
            DropForeignKey("dbo.Promotion", "ProduitId", "dbo.Produit");
            DropForeignKey("dbo.PackProduct", "ProduitId", "dbo.Produit");
            DropForeignKey("dbo.PackProduct", "PackId", "dbo.Pack");
            DropForeignKey("dbo.Produit", "MagasinId", "dbo.Magasin");
            DropIndex("dbo.Sortie", new[] { "produit_ProduitId" });
            DropIndex("dbo.Reclamation", new[] { "ProduitFK" });
            DropIndex("dbo.Reclamation", new[] { "AdminFK" });
            DropIndex("dbo.Reclamation", new[] { "ClientFK" });
            DropIndex("dbo.Reservation", new[] { "UserId" });
            DropIndex("dbo.Reservation", new[] { "EventId" });
            DropIndex("dbo.Rating", new[] { "EventId" });
            DropIndex("dbo.Entrée", new[] { "produit_ProduitId" });
            DropIndex("dbo.Comments", new[] { "ID_Post" });
            DropIndex("dbo.Promotion", new[] { "ProduitId" });
            DropIndex("dbo.PackProduct", new[] { "PackId" });
            DropIndex("dbo.PackProduct", new[] { "ProduitId" });
            DropIndex("dbo.Produit", new[] { "MagasinId" });
            DropIndex("dbo.Produit", new[] { "CategProdFK" });
            DropTable("dbo.Sortie");
            DropTable("dbo.Reclamation");
            DropTable("dbo.User");
            DropTable("dbo.Reservation");
            DropTable("dbo.Rating");
            DropTable("dbo.Event");
            DropTable("dbo.Entrée");
            DropTable("dbo.Posts");
            DropTable("dbo.Comments");
            DropTable("dbo.Promotion");
            DropTable("dbo.Pack");
            DropTable("dbo.PackProduct");
            DropTable("dbo.Magasin");
            DropTable("dbo.Produit");
            DropTable("dbo.CategorieProd");
        }
    }
}
