namespace crm.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Hazem : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Produit", "DateAjout", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Produit", "DateAjout");
        }
    }
}
