﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crm.Data.infrastructure
{
    public class DataBaseFactory : Disposable, IDataBaseFactory
    {
        CrmContext ctxt;

        public CrmContext Ctxt
        {
            get
            {
                return ctxt;
            }
        }

        public DataBaseFactory()
        {
            ctxt = new CrmContext();
        }

        public override void DisposeCore()
        {
            if (ctxt != null)
                ctxt.Dispose();
        }
    }
}
