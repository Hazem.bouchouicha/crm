﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crm.Data.infrastructure
{
    public interface IDataBaseFactory : IDisposable
    {
        CrmContext Ctxt { get; }
    }
}
