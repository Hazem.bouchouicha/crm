﻿using crm.Data.configurations;
using crm.Data.conventions;
using crm.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crm.Data
{
    public class CrmContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Produit> produits { get; set; }
        public DbSet<CategorieProd> categorieProds { get; set; }
        public DbSet<Entrée> entrées { get; set; }
        public DbSet<Sortie> sorties { get; set; }
        public DbSet<Magasin> magasins { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<Reservation> Reservations { get; set; }
        public DbSet<Rating> Ratings { get; set; }
        public DbSet<Reclamation> Reclamations { get; set; }
        public DbSet<Promotion> promotions { get; set; }
        public DbSet<Pack> packs { get; set; }
        public DbSet<PackProduct> packproducts { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Comment> Comments { get; set; }






        public CrmContext() : base("Name=DefaultConnection")

        {


            Database.SetInitializer<CrmContext>(new DropCreateDatabaseIfModelChanges<CrmContext>());

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Add(new DateConvention());
            modelBuilder.Configurations.Add(new CategConfig());
            modelBuilder.Configurations.Add(new MagasinConfig());
            modelBuilder.Configurations.Add(new ProduitConfig());
            modelBuilder.Configurations.Add(new PackConfig());
            /* modelBuilder.Entity<IdentityUserLogin>().HasKey<string>(l => l.UserId);
             modelBuilder.Entity<IdentityRole>().HasKey<string>(r => r.Id);
             modelBuilder.Entity<IdentityUserRole>().HasKey(r => new { r.RoleId, r.UserId });
             */



            // configures one-to-many relationship
            modelBuilder.Entity<Reservation>()
                .HasRequired<Event>(s => s.Event)
                .WithMany(g => g.Reservations)
                .HasForeignKey<int>(s => s.EventId);


            modelBuilder.Entity<Reservation>()
                .HasRequired<User>(s => s.User)
                .WithMany(g => g.Reservations)
                .HasForeignKey<int>(s => s.UserId);


        }



    }
}
