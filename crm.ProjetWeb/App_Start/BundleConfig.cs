﻿using System.Web;
using System.Web.Optimization;

namespace crm.ProjetWeb
{
    public class BundleConfig
    {
        // Pour plus d'informations sur le regroupement, visitez https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Utilisez la version de développement de Modernizr pour le développement et l'apprentissage. Puis, une fois
            // prêt pour la production, utilisez l'outil de génération à l'adresse https://modernizr.com pour sélectionner uniquement les tests dont vous avez besoin.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            #region adminTemplate


            bundles.Add(new ScriptBundle("~/templateAdmin/js").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new StyleBundle("~/templateAdmin/css").Include(
                     "~/Content/css/bootstrap.min.css",
                    "~/fonts/fonts/circular-std/style.css",
                    "~/Content/css/style.css",
                    "~/fonts/fonts/fontawesome/css/fontawesome-all.css",
                    "~/Content/charts/chartist-bundle/chartist.css",
                    "~/Content/charts/morris-bundle/morris.css",
                    "~/fonts/fonts/material-design-iconic-font/css/materialdesignicons.min.css",
                    "~/Content/charts/c3charts/c3.css",
                    "~/fonts/fonts/flag-icon-css/flag-icon.min.css"));


            #endregion
        }
    }
}
