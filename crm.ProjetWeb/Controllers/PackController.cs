﻿using crm.Data.infrastructure;
using crm.Domain.Entity;
using crm.Services;
using crm.ServicesPattern;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace crm.ProjetWeb.Controllers
{
    public class PackController : Controller
    {
        IPackService Service = new PackService();
        // GET: Pack
        public ActionResult Index()
        {
            IDataBaseFactory Factory = new DataBaseFactory();
            IUnitOfWork Uok = new UnitOfWork(Factory);
            IService<Pack> PackService = new Service<Pack>(Uok);

            return View(PackService.GetAll().ToList());
        }

        // GET: Pack/Details/5
        public ActionResult Details(int id)
        {
            Pack cm = new Pack();
            cm = Service.GetById(id);
            Pack c = new Pack();


            c.Description = cm.Description;
            c.NomPack = cm.NomPack;
            c.PackDateDebut = cm.PackDateDebut;
            c.PackDateFin = cm.PackDateFin;
            c.PackId = cm.PackId;
            c.PackPrix = cm.PackPrix;
            c.PromoImage = cm.PromoImage;


            return View(c);
        }

        // GET: Pack/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Pack/Create
        [HttpPost]
        public ActionResult Create(Pack pr, HttpPostedFileBase Image)
        {
            pr.PromoImage = Image.FileName;
            var path = Path.Combine(Server.MapPath("~/Content/Upload/"), Image.FileName);
            Image.SaveAs(path);

            






            Service.Add(pr);
            Service.Commit();





            return RedirectToAction("Index");
        }

        // GET: Pack/Edit/5
        public ActionResult Edit(int id)
        {
            if (id == null)

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Pack p = Service.GetById(id);
            Pack p1 = new Pack();
            {

                p1.PackId = p.PackId;
                p1.NomPack = p.NomPack;
                p1.PackDateDebut = p.PackDateDebut;
                p1.PackDateFin = p.PackDateFin;
                p1.Description = p.Description;
                p1.PromoImage = p.PromoImage;
                p1.PackPrix = p.PackPrix;


            };
            if (p == null)
                return HttpNotFound();
            return View(p1);

        

        
         }

        // POST: Pack/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, Pack pr)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (id == null)
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    Pack p = Service.GetById(id);


                    p.PackId = pr.PackId;
                    p.NomPack = pr.NomPack;
                    p.PackDateDebut = pr.PackDateDebut;
                    p.PackDateFin = pr.PackDateFin;
                    p.Description = pr.Description;
                    p.PromoImage = pr.PromoImage;
                    p.PackPrix = pr.PackPrix;



                    if (p == null)
                        return HttpNotFound();

                    Service.Update(p);
                    Service.Commit();
                    // Service.Dispose();

                    return RedirectToAction("Index");
                }
                // TODO: Add delete logic here
                //  return View(ovm);

            }
            catch
            {
                return View("Index");
            }
            return null;

        }

        // GET: Pack/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Pack/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            Pack e;
            e = Service.GetById(id);

            Service.Delete(e);
            Service.Commit();
            //Service.dispose();

            return RedirectToAction("Index");
        }
    }
}
