﻿using crm.Data.infrastructure;
using crm.Domain.Entity;
using crm.ProjetWeb.Models;
using crm.Services;
using crm.ServicesPattern;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace crm.ProjetWeb.Controllers
{
    public class PromotionController : Controller
    {


        IPromotionService Service = new PromotionService();

        // GET: Promotion
        public ActionResult Index()
        {
            IDataBaseFactory Factory = new DataBaseFactory();
            IUnitOfWork Uok = new UnitOfWork(Factory);
            IService<Promotion> ProService = new Service<Promotion>(Uok);

            return View(ProService.GetAll().ToList());
        }

        // GET: Promotions/Details/5
        public ActionResult Details(int id)
        {
            Promotion cm = new Promotion();
            cm = Service.GetById(id);
            Promotion c = new Promotion();


            c.ProduitId = cm.ProduitId;
            c.NomPromo = cm.NomPromo;
            c.PrDateDebut = cm.PrDateDebut;
            c.PrDateFin = cm.PrDateFin;
            c.PromoId = cm.PromoId;
            c.PromoPrix = cm.PromoPrix;
            c.PromoImage = cm.PromoImage;


            return View(c);
        }

        // GET: Promotion/Create
        public ActionResult Create()
        {
            
            IProduitService m = new ProduitService();
            
           
            var listDesProduits = m.GetAll();

            
            ViewBag.listDesProduits = new SelectList(listDesProduits, "ProduitId", "nomProd");




            return View();
        }


       

        // POST: Promotion/Create
        [HttpPost]
        public ActionResult Create(Promotion pr, HttpPostedFileBase Image)
        {



            pr.PromoImage = Image.FileName;
            var path = Path.Combine(Server.MapPath("~/Content/Upload/"), Image.FileName);
            Image.SaveAs(path);


            //if (!ModelState.IsValid || Image == null || Image.ContentLength == 0)
            //{
            //    RedirectToAction("Create");
            //}

            //Service.Add(new Promotion()
            //{
            //    PrDateDebut = pr.PrDateDebut.T,
            //    location = e.location


            //});
            //es.Commit();

           





            Service.Add(pr);
            Service.Commit();





            return RedirectToAction("Index");
        }





        // GET: Promotion/Edit/5
        public ActionResult Edit(int id)
        {

            IProduitService m = new ProduitService();
            
           
            var listDesProduits = m.GetAll();

            
            ViewBag.listDesProduits = new SelectList(listDesProduits, "ProduitId", "nomProd");


            
            if (id == null)

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Promotion p = Service.GetById(id);
            Promotion p1 = new Promotion();
            {

                p1.PromoId = p.PromoId;
                p1.NomPromo = p.NomPromo;
                p1.PrDateDebut = p.PrDateDebut;
                p1.PrDateFin = p.PrDateFin;
                p1.Description = p.Description;
                p1.PromoImage = p.PromoImage;
                p1.PromoPrix = p.PromoPrix;
                p1.ProduitId = p.ProduitId;

            };
            if (p == null)
                return HttpNotFound();
            return View(p1);

            return RedirectToAction("Index");

        }

        // POST: Promotion/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, Promotion pr)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (id == null)
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    Promotion p = Service.GetById(id);


                    p.PromoId = pr.PromoId;
                    p.NomPromo = pr.NomPromo;
                    p.PrDateDebut = pr.PrDateDebut;
                    p.PrDateFin = pr.PrDateFin;
                    p.Description = pr.Description;
                    p.PromoImage = pr.PromoImage;
                    p.PromoPrix = pr.PromoPrix;



                    if (p == null)
                        return HttpNotFound();

                    Service.Update(p);
                    Service.Commit();
                    // Service.Dispose();

                    return RedirectToAction("Index");
                }
                // TODO: Add delete logic here
                //  return View(ovm);

            }
            catch
            {
                return View("Index");
            }
            return null;

        }
            public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Promotion/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            Promotion e;
            e = Service.GetById(id);

            Service.Delete(e);
            Service.Commit();
            //Service.dispose();

            return RedirectToAction("Index");
        }
    }
}
