﻿using crm.Domain.Entity;
using crm.ProjetWeb.Models;
using crm.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace crm.ProjetWeb.Controllers
{
    public class ReclamationController : Controller
    {
        //private Data.CrmContext db = new Data.CrmContext();
        IReclamationService cs = new ReclamationService();
        IProduitService ps = new ProduitService();
        IUserService us = new UserService();
        // GET: Reclamation
        public ActionResult Index()
        {
            //  var reclamations=db.Reclamations.Include(p=> p.Users);
            var reclamations = cs.GetAll();
            List<ReclamationModel> ListReclamationModel = new List<ReclamationModel>();
            
            foreach (var rec in reclamations)
            {
                ReclamationModel rm = new ReclamationModel();
                if (rec.ProduitFK != null) {
                    rm.NomProduit = ps.GetById(rec.ProduitFK).nomProd;
                }
                rm.Reclamationid = rec.Reclamationid;
                rm.NomClient = us.GetById(rec.ClientFK).Nom;
                rm.type = rec.type;
                rm.reponse = rec.reponse;
                rm.etat = rec.etat;
                rm.reponse = rec.reponse;
                rm.description = rec.description;
                rm.DateReclamation = rec.DateReclamation;
                ListReclamationModel.Add(rm);
            }

            return View(ListReclamationModel);
        }

        // GET: Reclamation/Details/5
        public ActionResult Details(int id)
        {
            Reclamation p = new Reclamation();
            p = cs.Get(t => t.Reclamationid == id);
            ReclamationModel pm = new ReclamationModel();
            pm.Reclamationid = p.Reclamationid;
            pm.type = p.type;
            pm.etat = p.etat;
            pm.description = p.description;
            pm.reponse = p.reponse;
            pm.DateReclamation = p.DateReclamation;

            return View(pm);
        }
        // POST: Reclamation/Details/5
        [HttpPost]
        public ActionResult Details(int id, ReclamationModel c)
        {
            Reclamation cm = new Reclamation();
            cm = cs.Get(t => t.Reclamationid == id);

            cm.etat ="En cours";


            cs.Update(cm);
            cs.Commit();
            return RedirectToAction("Index");
        }
    
        [HttpPost]
        public ActionResult Refuser(int id, ReclamationModel cm)
        {
            Reclamation c = new Reclamation();
            c = cs.Get(t => t.Reclamationid == id);
            c.etat = "Refusée";
            cs.Update(c);
            cs.Commit();
            return RedirectToAction("Create");
        }
        // GET: Reclamation/Delete/5
        public ActionResult Delete(int id)
        {
            Models.ReclamationModel c = new Models.ReclamationModel();
            Domain.Entity.Reclamation cm = new Domain.Entity.Reclamation();
            cm = cs.Get(t => t.Reclamationid == id);
            c.Reclamationid = id;
            c.type = cm.type;
            c.description = cm.description;
            c.etat = cm.etat;
            c.DateReclamation = cm.DateReclamation;
            return View(c);
        }
        // POST: Reclamation/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, Models.ReclamationModel cm)
        {
            Domain.Entity.Reclamation c = new Domain.Entity.Reclamation();
            c = cs.Get(t => t.Reclamationid == id);
            cs.Delete(c);
            cs.Commit();
            return RedirectToAction("Index");
        }
       
        // GET: Reclamation/Create
        public ActionResult Create()
        {
            IProduitService ps = new ProduitService();
            var produits = ps.GetAll();
            ViewBag.ListeProduits = new SelectList(produits, "ProduitId", "nomProd");
            return View();
        }

        // POST: Reclamation/Create
        [HttpPost]
        public ActionResult Create(Models.ReclamationModel rm)
        {
            Domain.Entity.Reclamation r = new Domain.Entity.Reclamation();
            r.type = rm.type;
            r.description = rm.description;
            r.ClientFK = 1;
            r.AdminFK = 2;
            if (r.type.ToString() == "Technique")
            {
                r.ProduitFK = rm.ProduitFK;
            }

            r.etat = "Non traitée";
            r.DateReclamation = DateTime.Today;
            cs.Add(r);
            cs.Commit();
            return RedirectToAction("Index");

        }

        /* Liste des projets du client connecté */
        public ActionResult NombreReclamationsSurProduit(int id)
        {
            Produit p = new Produit();
            var a = 0;
            p = ps.Get(t => t.ProduitId == id);
            List<Models.ReclamationModel> list = new List<Models.ReclamationModel>();
            foreach (var item in cs.GetAll().Where(t => t.ProduitFK == id))
            {
                a++;

                ViewData["Nombre"] = a;

            }

            return View();

        }
        // GET: Reclamation/Edit/5
        public ActionResult Edit(int id)
        {
            Reclamation cm = new Reclamation();

            cm = cs.Get(t => t.Reclamationid == id);
            ReclamationModel c = new ReclamationModel();
            c.Reclamationid = cm.Reclamationid;
            c.type = cm.type;
            c.description = cm.description;
            c.DateReclamation = cm.DateReclamation;
            c.etat = cm.etat;
            c.reponse = cm.reponse;
            return View(c);
        }

        // POST: Reclamation/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, ReclamationModel cm)
        {
            Reclamation c = new Reclamation();
            c = cs.Get(t => t.Reclamationid == id);
            c.Reclamationid = cm.Reclamationid;
            c.type = cm.type;
            c.description = cm.description;
            c.etat = cm.etat;
            c.reponse = cm.reponse;


            cs.Update(c);
            cs.Commit();
            return RedirectToAction("Index");

        }
        // GET: Reclamation/Repondre/5
        public ActionResult Repondre(int id)
        {

            Reclamation cm = new Reclamation();

            cm = cs.Get(t => t.Reclamationid == id);
            ReclamationModel c = new ReclamationModel();
            c.Reclamationid = cm.Reclamationid;
            c.type = cm.type;
            c.description = cm.description;
            c.DateReclamation = cm.DateReclamation;
            c.etat = cm.etat;
            c.reponse = cm.reponse;
            return View(c);
        }
        // POST: Reclamation/Repondre/5
        [HttpPost]
        public ActionResult Repondre(int id, Reclamation c)
        {
            Reclamation cm = new Reclamation();
            cm = cs.Get(t => t.Reclamationid == id);
            cm.etat = "Traitée";
            cm.reponse = c.reponse;

          
            cs.Update(cm);
            cs.Commit();
            return RedirectToAction("Index");
        }
    }
}
/*
 public ActionResult Index()  
        {  
            return View();  
        }  
         
     
*/
