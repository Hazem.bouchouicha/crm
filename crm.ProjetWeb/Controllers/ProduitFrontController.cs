﻿using crm.Domain.Entity;
using crm.ProjetWeb.Models;
using crm.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace crm.ProjetWeb.Controllers
{
    public class ProduitFrontController : Controller
    {


     
        IProduitService Service = new ProduitService();


        ICategProduitService c = new CategProduitService();
        IMagasinService m = new MagasinService();



      

        // GET: ProduitFront
        public ActionResult Index()
        {

           



            var produits = Service.GetAll();
            List<ProduitModel> prodModel = new List<ProduitModel>();
            //ProduitModel pm = new ProduitModel();
            foreach (var p in produits)
            {
                ProduitModel pm = new ProduitModel();
                pm.CatName = c.GetById(p.CategProdFK).nomCategorie;
                pm.MagasinName = m.GetById(p.MagasinId).nomMagasin;
                pm.prix = p.prix;
                pm.ProduitId = p.ProduitId;
                pm.qte = p.qte;
                pm.qteMin = p.qteMin;
                pm.unite = p.unite;
                pm.description = p.description;
                pm.image = p.image;
                pm.nomProduit = p.nomProd;
                prodModel.Add(pm);

            }

            return View(prodModel);

            //   return View(dbSetPointDeVenteMobile.ToList())
        }



        // GET: ProduitFront/Details/5
        public ActionResult Details(int id)
        {
            
            
            var prod = Service.GetById(id);
            ViewBag.quantite = prod.qte;

            return View(prod);
        }



        
    }
}
