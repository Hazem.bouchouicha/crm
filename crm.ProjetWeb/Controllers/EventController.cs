﻿using crm.Domain.Entity;
using crm.Services;
using CrmWeb.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;

using System.Web;
using System.Web.Mvc;
namespace CrmWeb.Controllers
{


    public class EventController : Controller
    {
        crm.Data.CrmContext ctx;

        IEventService evs = new EventService();

        // GET: Event
        public ActionResult Index()
        {
            var listEvent = evs.GetMany();
            var events = new List<Event>();

            foreach (Event e in listEvent)
            {
                events.Add(new Event()
                {
                    EventId = e.EventId,
                    Nom_Event = e.Nom_Event,
                    Nb_Place = e.Nb_Place,
                    Place = e.Place,
                    type = e.type,
                    dateDebut = e.dateDebut,
                });
            }

            return View(events);

        }
        // GET: Event
        public ActionResult Create()
        {
            return View();
        }
        // POST: Event
        [HttpPost]
        public ActionResult Create(Event e, HttpPostedFileBase File)
        {

            //ce if va vérifier si le modéle est valide et que le file n'est pas vide(ou null)
            if (!ModelState.IsValid || File == null || File.ContentLength == 0)
            {
                RedirectToAction("Create");

            }
            evs.Add(new Event()
            {
                EventId = e.EventId,
                Nom_Event = e.Nom_Event,
                Nb_Place = e.Nb_Place,
                Place = e.Place,
                type = e.type,
                dateDebut = e.dateDebut

            });
            evs.Commit();

            return RedirectToAction("Index");
        }

        // GET: Event/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }



        // GET: Event/Edit/5
        public ActionResult Edit(int id)
        {
            EventModel model = new EventModel();
            var e = evs.GetById(id);
            model.dateDebut = e.dateDebut;
            model.EventId = e.EventId;
            model.Nb_Place = e.Nb_Place;
            model.Nom_Event = e.Nom_Event;
            model.type = e.type;
            model.Place = e.Place;
            return View(model);
        }

        // POST: Event/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Event ev)
        {
            Console.WriteLine(ev);
            try
            {
                //ce if va vérifier si le modéle est valide et que le file n'est pas vide(ou null)
                Event e = evs.GetById(id);
                // ctx.Entry(ev).State = EntityState.Modified;
                // ctx.SaveChanges();


                e.EventId = ev.EventId;
                e.Nom_Event = ev.Nom_Event;
                e.Nb_Place = ev.Nb_Place;
                e.Place = ev.Place;
                e.type = ev.type;
                e.dateDebut = ev.dateDebut;

                evs.Update(e);
                evs.Commit();
                return RedirectToAction("Index");





            }
            catch { return View(); }
        }
        // GET: Event/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Event/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                evs.Delete(x => x.EventId == id);
                evs.Commit();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
