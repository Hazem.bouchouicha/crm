﻿using crm.Domain.Entity;
using crm.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace crm.ProjetWeb.Controllers
{
    public class ReservationController : Controller
    {
     IReservationService rs = new ReservationService();
        // GET: Reservation
        //liste event: instance serv event + affichage --> view feha reserver button (create post) 
        public ActionResult IndexResEvent(string str)
        {
            IEventService evs = new EventService();
            var listEvent = evs.GetMany();
            if(str != null)
            {
                var events = new List<Event>();
                foreach (Event e in listEvent)
                {

                    if (e.Nom_Event.Contains(str) || e.Place.Contains(str) || e.type.Contains(str)) {
                        events.Add(e);
                    }
                  }
                return View(events);

                /* if(searchAction=="Name")
                  { return View(evs.Get(x => x.Nom_Event == searchAction)); }
                  */

            }
            return View(listEvent);
            
        }
        public ActionResult AjouterReservation(int id)
        { 
            EventService evs = new EventService();
            IUserService user_serv = new UserService();
            ReservationService resser = new ReservationService();


            User u = user_serv.GetById(1);
            Event ev = evs.GetById(id);
            Reservation res = new Reservation();

            ev.Nb_Place--;
            evs.Update(ev);
            evs.Commit();

   

            res.EventId = id;
            res.UserId = 1;
           // res.ResId = 1;

            //     res.ResId = resser.GetAll().Count()+10;
            // res.EventId = id;

            //int userId = int.Parse(User.Identity.GetUserId());//getconnected user
            //  int userId = User.Identity.IsAuthenticated.CompareTo(1);
            // int userId = 3;
           
            resser.Add(res);

            resser.Commit();


            

            return RedirectToAction("IndexResEvent");

        }



        public ActionResult IndexMesRes()
        {
            int userId = 1;
            ReservationService serv_res = new ReservationService();
            var lst_res_src = serv_res.GetAll();
            var liste_mes_res = new List<Reservation>();

            foreach (Reservation e in lst_res_src)
            {
                    if (e.UserId == userId)
                    {
                        liste_mes_res.Add(e);
                    }
             }
                        


            return View(liste_mes_res);
            
        }

        // GET: Reservation/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Reservation/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Reservation/Create
        [HttpPost]
        public ActionResult Create(Reservation e, HttpPostedFileBase File)
        {

            //ce if va vérifier si le modéle est valide et que le file n'est pas vide(ou null)
            if (!ModelState.IsValid || File == null || File.ContentLength == 0)
            {
                RedirectToAction("Create");

            }
            rs.Add(new Reservation
            {
                ResId =e.ResId,
                EventId = e.EventId,
                UserId = e.UserId,
            });
            rs.Commit();

            return RedirectToAction("Index");
        }

        // GET: Reservation/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Reservation/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Reservation/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }
             
        // POST: Reservation/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                EventService evs = new EventService();

                Reservation data = rs.GetById(id);
                Event e = evs.GetById(data.EventId);

                e.Nb_Place++;
                evs.Update(e);
                evs.Commit();



                rs.Delete(x => x.ResId == id);
                rs.Commit();



                return RedirectToAction("IndexMesRes");
            }
            catch
            {
                return View();
            }
        }
        //  public ActionResult SearchEvent(int id) { }

    }
}
