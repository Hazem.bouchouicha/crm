﻿using crm.Data.infrastructure;
using crm.Domain.Entity;
using crm.Services;
using crm.ServicesPattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace crm.ProjetWeb.Controllers
{

   
    public class PackProductController : Controller
    {
        IPackProductService Service = new PackProductService();

        // GET: PackProduct
        public ActionResult Index()
        {
            IDataBaseFactory Factory = new DataBaseFactory();
            IUnitOfWork Uok = new UnitOfWork(Factory);
            IService<PackProduct> ProService = new Service<PackProduct>(Uok);

            return View(ProService.GetAll().ToList());
        }







        // GET: PackProduct/Details/5
        public ActionResult Details(int id)
        {
            PackProduct cm = new PackProduct();
            cm = Service.GetById(id);
            PackProduct c = new PackProduct();


            c.ProduitId = cm.ProduitId;
            c.PackId = cm.PackId;


            return View(c);
        }








        // GET: PackProduct/Create
        public ActionResult Create()
        {
            IProduitService m = new ProduitService();


            var listDesProduits = m.GetAll();


            ViewBag.listDesProduits = new SelectList(listDesProduits, "ProduitId", "nomProd");




            IPackService p = new PackService();


            var listDesPack = p.GetAll();


            ViewBag.listDesPack = new SelectList(listDesPack, "PackId", "NomPack");



            return View();
        }











        // POST: PackProduct/Create
        [HttpPost]
        public ActionResult Create(PackProduct pro, HttpPostedFileBase Image)
        {
            Service.Add(pro);
            Service.Commit();





            return RedirectToAction("Index");
        }





        // GET: PackProduct/Edit/5
        public ActionResult Edit(int id)
        {
            IProduitService m = new ProduitService();


            var listDesProduits = m.GetAll();


            ViewBag.listDesProduits = new SelectList(listDesProduits, "ProduitId", "nomProd");




            IPackService p = new PackService();


            var listDesPack = p.GetAll();


            ViewBag.listDesPack = new SelectList(listDesPack, "PackId", "NomPack");

            if (id == null)

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            PackProduct p0 = Service.GetById(id);
            PackProduct p1 = new PackProduct();
            {

                p1.PackId = p0.PackId;
                p1.ProduitId = p0.ProduitId;
                

            };
            if (p == null)
                return HttpNotFound();
            return View(p1);

            return RedirectToAction("Index");
        }






        // POST: PackProduct/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, PackProduct pr)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (id == null)
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    PackProduct p = Service.GetById(id);


                    p.ProduitId = pr.ProduitId;
                    p.PackId = pr.PackId;
                    


                    if (p == null)
                        return HttpNotFound();

                    Service.Update(p);
                    Service.Commit();
                    // Service.Dispose();

                    return RedirectToAction("Index");
                }
                // TODO: Add delete logic here
                //  return View(ovm);

            }
            catch
            {
                return View("Index");
            }
            return null;
        }

        // GET: PackProduct/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: PackProduct/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            PackProduct e;
            e = Service.GetById(id);

            Service.Delete(e);
            Service.Commit();
            //Service.dispose();

            return RedirectToAction("Index");
        }
    }
}
