﻿using crm.Data;
using crm.Domain.Entity;
using crm.ProjetWeb.Models;
using crm.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace crm.ProjetWeb.Controllers
{
    public class ProduitController : Controller
    {
        CrmContext crm = new CrmContext();
        IProduitService Service = new ProduitService();


        ICategProduitService c = new CategProduitService();
        IMagasinService m = new MagasinService();


        // GET: Produit
        public ActionResult Index()
        {



           // int nbrProd = Service.NbrProduits();
           // ViewBag.Nombre = nbrProd;



            var produits = Service.GetAll();
            List<ProduitModel> prodModel = new List<ProduitModel>();
            //ProduitModel pm = new ProduitModel();
            foreach (var p in produits)
            {
                ProduitModel pm = new ProduitModel();
                pm.CatName = c.GetById(p.CategProdFK).nomCategorie;
                pm.MagasinName = m.GetById(p.MagasinId).nomMagasin;
                pm.prix = p.prix;
                pm.ProduitId = p.ProduitId;
                pm.qte = p.qte;
                pm.qteMin = p.qteMin;
                pm.unite = p.unite;
                pm.description = p.description;
                pm.image = p.image;
                pm.nomProduit = p.nomProd;
                prodModel.Add(pm);

            }

            return View(prodModel);

            //   return View(dbSetPointDeVenteMobile.ToList())
        }


        [HttpPost]
        public ActionResult Index(string filtre)
        {
            var produits = Service.GetAll();
            List<ProduitModel> prodModel = new List<ProduitModel>();
            foreach (var p in produits)
            {
                ProduitModel pm = new ProduitModel();
                pm.CatName = c.GetById(p.CategProdFK).nomCategorie;
                pm.MagasinName = m.GetById(p.MagasinId).nomMagasin;
                pm.prix = p.prix;
                pm.qte = p.qte;
                pm.qteMin = p.qteMin;
                pm.unite = p.unite;
                pm.description = p.description;
                pm.image = p.image;
                pm.nomProduit = p.nomProd;
                prodModel.Add(pm);
            }

            //var list = Service.GetAll();


            // filtrage 
            if (!String.IsNullOrEmpty(filtre))
            {
                prodModel = prodModel.Where(m => m.nomProduit.Equals(filtre)).ToList();
            }

            return View(prodModel);



        }








        // GET: Produit/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Produit/Create
        public ActionResult Create()
        {
            ICategProduitService c = new CategProduitService();
            IMagasinService m = new MagasinService();


            var categorieProds = c.GetAll();
            var magasins = m.GetAll();

            ViewBag.CategList = new SelectList(categorieProds, "CategorieProdid", "nomCategorie");
            ViewBag.magasinList = new SelectList(magasins, "MagasinId", "nomMagasin");




            return View();
        }

        // POST: Produit/Create
        [HttpPost]
        public ActionResult Create(Produit p, HttpPostedFileBase Image)
        {



            p.image = Image.FileName;
            var path = Path.Combine(Server.MapPath("~/Content/Upload/"), Image.FileName);
            Image.SaveAs(path);
            p.DateAjout = DateTime.Now;


            Service.Add(p);
            Service.Commit();





            return RedirectToAction("Index");
        }



        // GET: Produit/Edit/5
        public ActionResult Edit(int id)
        {
            ICategProduitService c = new CategProduitService();
            IMagasinService m = new MagasinService();
            Produit e = Service.GetById(id);
            var categorieProds = c.GetAll();
            var magasins = m.GetAll();
            ViewBag.CategList = new SelectList(categorieProds, "CategorieProdid", "nomCategorie");
            ViewBag.magasinList = new SelectList(magasins, "MagasinId", "nomMagasin");

            return View(e);
        }

        // POST: Produit/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Produit p)
        {
            try
            {
                Produit c1 = Service.GetById(id);

                c1.nomProd = p.nomProd;
                c1.prix = p.prix;
                c1.qte = p.qte;
                c1.qteMin = p.qteMin;
                c1.unite = p.unite;
                c1.MagasinId = p.MagasinId;
                c1.CategProdFK = p.CategProdFK;


                Service.Update(c1);
                Service.Commit();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Produit/Delete/5
        public ActionResult Delete(int id)
        {

            var c1 = Service.GetById(id);

            return View(c1);
        }

        // POST: Produit/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                Produit c1 = Service.GetById(id);
                Service.Delete(c1);
                Service.Commit();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}