﻿using crm.Data;
using crm.Domain.Entity;
using crm.ProjetWeb.Models;
using crm.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace crm.ProjetWeb.Controllers
{
    public class PostsController : Controller
    {
        CrmContext crm = new CrmContext();
        IPostService es = new PostService();
        ICommentService ec = new CommentService();
        // GET: Posts
        public ActionResult Index(string searchString)
        {
            var movies = from m in crm.Posts
                         select m;

            if (!String.IsNullOrEmpty(searchString))
            {
                movies = movies.Where(s => s.body.Contains(searchString));
            }

            return View(movies);
        }

        // GET: Posts/Details/5
        public ActionResult Details(int id)
        {
            crm.Domain.Entity.Post pDomain = es.GetById(id);
            PostModel p = new PostModel();
            if (p != null)
            {
                p.Title = pDomain.Title;
                p.body = pDomain.body;
                p.likes = pDomain.likes;
                //foreach (X in p.comments)
                //{
                //    p.comments = pDomain.comments; }
                ViewBag.id = pDomain.id;
            }

            return View(p);
        }

        // GET: Posts/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Posts/Create
        [HttpPost]
        public ActionResult Create(Post p , HttpPostedFileBase Image)
        {
            PostModel pm = new PostModel();
            if (p.Image != null)
            {
                try
                {
                    p.Title = pm.Title;
            p.body = pm.body;
            p.date = DateTime.Today;



            
                    string path = Path.Combine(Server.MapPath("~/Images"),
                                               Path.GetFileName(Image.FileName));
                    Image.SaveAs(path);
                    ViewBag.Message = "File uploaded successfully";
                }
                catch (Exception ex)
                {
                    ViewBag.Message = "ERROR:" + ex.Message.ToString();
                }
            }
            else
            {
                ViewBag.Message = "You have not specified a file.";
            }
           

            es.Add(p);
            es.Commit();





            return RedirectToAction("Index");
        }

        // GET: Posts/Edit/5
        public ActionResult Edit(int id)
        {
            Post p = es.GetById(id);

            return View(p);
        }

        // POST: Posts/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, PostModel c)
        {
            Post cm = new Post();
            cm = es.Get(t => t.id == id);
            if (cm != null)
            {
                cm.Title = c.Title;
                cm.body = c.body;
                es.Update(cm);
                es.Commit();
            }
            return RedirectToAction("Index");
        }

        // POST: Posts/Edit/5
        [HttpPost]
        public ActionResult EditLikes(int id, PostModel c)
        {
            Post cm = new Post();
            cm = es.Get(t => t.id == id);
            if (cm != null)
            {
                cm.likes = cm.likes + 1;
                es.Update(cm);
                es.Commit();
            }
            return RedirectToAction("Details");
        }

        // POST:Posts/Aime/5
        [HttpPost]
        public ActionResult Aime(int id, PostModel c)
        {
            //Post cm = new Post();
            //cm = es.Get(t => t.id == id);
            //if (cm != null)
            //{
            //    cm.likes = cm.likes + 1;
            //    es.Update(cm);
            //    es.Commit();
            //}
            return RedirectToAction("Details");
        }




        // GET: Posts/Delete/5
        public ActionResult Delete(int id)
        {
            var c1 = es.GetById(id);

            return View(c1);
        }

        // POST: Posts/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {

            try
            {
                Post c1 = es.GetById(id);
                es.Delete(c1);
                es.Commit();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Posts/Edit/5
        public ActionResult moveComment(int id)
        {
            Post p = es.GetById(id);

            return View(p);
        }
        // POST: Comment/Create
        public ActionResult addComment(int id, Comment e, HttpPostedFileBase File)
        {
            //ce if va vérifier si le modéle est valide et que le file n'est pas vide(ou null)
            if (!ModelState.IsValid || File == null || File.ContentLength == 0)
            {
                RedirectToAction("Create");
            }

            ec.Add(new Comment() { body = e.body, ID_Post = id });
            es.Commit();

            return View();
        }

        public ActionResult voir(int id, PostModel c)
        {
            //Post cm = new Post();
            //cm = es.Get(t => t.id == id);
            //if (cm != null)
            //{
            //    cm.likes = cm.likes + 1;
            //    es.Update(cm);
            //    es.Commit();
            //}
            return RedirectToAction("voir");
        }


        [HttpPost]
        public ActionResult commenter(CommentModel e)
        {
            Comment r = new Comment();
            r.body = e.body;
            r.ID_Post = 1;
            ec.Add(r);
            ec.Commit();
            return RedirectToAction("Index");

        }
    }

}