﻿using crm.Domain.Entity;
using crm.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace crm.ProjetWeb.Controllers
{
    public class CategProduitController : Controller
    {


        ICategProduitService service = new CategProduitService();

        // GET: CategProduit
        public ActionResult Index()
        {
            return View(service.GetAll());
        }

        // GET: CategProduit/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: CategProduit/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: CategProduit/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CategorieProd c)
        {
            try
            {

             
                service.Add(c);
                service.Commit();
               // service.dispose();

                return View();
            }
            catch
            {
                return View();
            }
        }

        // GET: CategProduit/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: CategProduit/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: CategProduit/Delete/5
        public ActionResult Delete(int id)
        {
           
         
            return View();
        }

        // POST: CategProduit/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                CategorieProd c1 = service.GetById(id);
                service.Delete(c1);
                service.Commit();

                return View("Index");
            }
            catch
            {
                return View("Index");
            }
        }
    }
}
