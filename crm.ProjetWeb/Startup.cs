﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(crm.ProjetWeb.Startup))]
namespace crm.ProjetWeb
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
