﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using crm.Domain.Entity;

namespace CrmWeb.Models
{
    [Table("Event")]
    public class EventModel
    {
        private Event ev;

        [Key]
        public int EventId { get; set; }


        public String Nom_Event { get; set; }

        public DateTime dateDebut { get; set; }

        public String Place { get; set; }
        public int Nb_Place { get; set; }
        public String type { get; set; }
        public List<UserModel> listUser { get; set; }

        public ICollection<ReservationModel> Reservations { get; set; }
        // public ICollection<RatingModel> RatingModel { get; set; }

        public EventModel()
        {

        }

        public EventModel(Event ev)
        {
            this.ev = ev;
        }
    }
}
