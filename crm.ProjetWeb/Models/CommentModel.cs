﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace crm.ProjetWeb.Models
{
    public class CommentModel
    {
        [Key]
        public int ID_Comment { get; set; }
        [Column("body")]
        [Required(ErrorMessage = "Please enter Comment Body.")]
        public string body { get; set; }
        public DateTime date { get; set; }
        public int? ID_Post { get; set; }
        [ForeignKey("ID_Post")]
        public virtual PostModel Post { get; set; }

        
    }
}