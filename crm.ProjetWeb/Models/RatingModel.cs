﻿using crm.Domain.Entity;
using CrmWeb.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crm.ProjetWeb.Models

{
    [Table("Rating")]

    public class Rating
    {
        [Key]
        public int RatingId { get; set; }

        public int Note { get; set; }
        public int? ResourceId { get; set; }
        [ForeignKey("EventId")]
        public virtual EventModel EventModel { get; set; }

    }
}
