﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrmWeb.Models
{
    [Table("Reservation")]
    public class ReservationModel
    {
        [Key]
        public int ResId { get;set ; }
      
        public int EventId { get; set; }
        public int UserId { get; set; }
        
        public virtual EventModel Event { get; set; }
        public virtual UserModel User { get; set; }

       // public virtual IdentityModels identite { get; set; }

        public ReservationModel()
        { }
    }
}
