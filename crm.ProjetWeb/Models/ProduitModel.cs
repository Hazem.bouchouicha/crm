﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace crm.ProjetWeb.Models
{
    public class ProduitModel
    {

    
        public int ProduitId { get; set; }
        public string nomProduit { get; set; }
        public string unite { get; set; }
        public int qte { get; set; }
        public int qteMin { get; set; }
        public int prix { get; set; }
        public string description { get; set; }
        public string image { get; set; }
        public int CategProdFK { get; set; }
        public string CatName { get; set; }
        public int MagasinId { get; set; }
        public string MagasinName { get; set; }

        public IEnumerable<SelectListItem> categories { get; set; }
        public IEnumerable<SelectListItem> magasins { get; set; }



    }
}