﻿using crm.Domain.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace crm.ProjetWeb.Models
{

    public class ReclamationModel
    {
        public int Reclamationid { get; set; }
        public TypeRec type { get; set; }
        public string description { get; set; }
        public string etat { get; set; }
        public int ClientFK { get; set; }
        public string NomClient { get; set; }
        public int AdminFK { get; set; }
        public int? ProduitFK { get; set; }
        public string NomProduit { get; set; }
        public string reponse { get; set; }
        public DateTime DateReclamation { get; set; }
        public IEnumerable<SelectListItem> produits { get; set; }
    }
}