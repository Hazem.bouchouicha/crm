﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrmWeb.Models
{


    [Table("User")]
   public class UserModel
    {

        [Key]
        public int UserId { get; set; }


        public String Nom { get; set; }

        public String Prenom { get; set; }

        public String Tel { get; set; }
        public String Adresse { get; set; }
        public String MDP { get; set; }
        public ICollection<ReservationModel> Reservations { get; set; }


        public UserModel()
        {

        }


    }
}
