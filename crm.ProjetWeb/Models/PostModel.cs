﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace crm.ProjetWeb.Models
{
    public class PostModel
    {
        [Key]
        public int id { get; set; }
        [Required(ErrorMessage = "Please enter Post Title.")]
        [ConcurrencyCheck]
        [MaxLength(24), MinLength(5)]
        public string Title { get; set; }
        [Required(ErrorMessage = "Please enter Post Body.")]
        public string body { get; set; }
        public DateTime date { get; set; }
        public String Image { get; set; }
        public String path { get; set; }
        public int likes { get; set; }
       
        public ICollection<CommentModel> comments { get; set; }
       
    }
}